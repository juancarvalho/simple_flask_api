#!/usr/bin/env python
# coding: utf-8

import requests
import json
import urlparse
import ConfigParser

# Get config vars ===========================================
config = ConfigParser.ConfigParser()  # Init config object
config.read('config/settings.ini')  # Set .ini file
env = config.get('environment', 'current')  # Get current environment
url_server = config.get(env, 'url_server')
create_url = config.get(env, 'create_url')
edit_url = config.get(env, 'edit_url')
get_url = config.get(env, 'get_url')


def insert(*args, **kwargs):
    """
        Create a post request to insert data in database
        Usage:
            insert(data='29/05/2019', valor='16566')
            insert(**{'data': '29/05/2019', 'valor': '16566'})
            insert(data='29/05/2019', **{'valor': '16566'})

        ARGS:
            keyword arg or dict

        RETURN:
            json object
    """
    url = urlparse.urljoin(url_server, create_url)
    return_insert = requests.post(url, json=kwargs)
    print(return_insert)
    print(return_insert.json())
    return return_insert.json()


def edit(*args, **kwargs):
    """
        Create a put request to edit data in database
        Usage:
            edit(_id='1273618237', data='29/05/2019')
            edit(**{'id': '12837648921', 'data':'22/02/1990'})
        ARGS:
            keyword arg or dict

        RETURN:
            json object
    """
    url = urlparse.urljoin(url_server, edit_url)
    return_edit = requests.put(url, json=kwargs)
    print(return_edit)
    print(return_edit.json())
    return return_edit.json()


def get(*args, **kwargs):
    """
        Create a get request to receive data from database
        Usage:
            get(valor=1200, data='29/05/2019')
            get(**{'valor': 1200, data':'22/02/1990'})
        ARGS:
            keyword arg or dict

        RETURN:
            json object
    """
    url = urlparse.urljoin(url_server, get_url)
    return_get = requests.get(url, json=kwargs)
    for data in json.loads(return_get.json()['detail']):
        print(data)
    return return_get.json()


get()


# edit(**{
#     '_id': "5d23c09115a587000d7ef67d",
#     # 'data': '01/07/2019',
#     # 'hora': '12:37:45',
#     # 'valor': 2000
#     })


# Enviar uma atring ao inves de date
# insert(**{
#     'data': '29/07/1982',
#     'hora': '14:52:12',
#     # 'containicial': '12877',
#     # 'contafinal': '0987',
#     'valor': 5000
#     })
