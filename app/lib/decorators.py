from flask import jsonify, request
from functools import wraps
import logging


def required_data(app, *args_req):
    """
        Search if receive required data

        ARGS:
            app > Flask object, only used to print logs
            args_req > str required values

        RETURN:
            function() or json object 
    """
    def decorator(func):
        @wraps(func)
        def wrapped(*args, **kwargs):
            keys = request.json.keys()  # Received keys in request
            for index, item in enumerate(args_req):
                if not item in keys:
                    return jsonify({
                        'success': False,
                        'detail': 'O dado "{}" é obrigatório.'.format(item)
                    }), 405
            return func(*args, **kwargs)
        return wrapped
    return decorator
