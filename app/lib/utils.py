from datetime import datetime


def convert_str_to_date(datestr):
    """
        Convert a str date in datetime.date
        usage:
            convert_date_str(d'2019-02-05')

        ARGS:
            str > date string

        RETURN:
            datetime.date and bool
    """
    date_aproved = False
    date_ret = None
    accept_formats = ["%d/%m/%Y %H:%M:%S"]
    for format_str in accept_formats:
        try:
            date_ret = datetime.strptime(datestr, format_str)
            date_aproved = True
            break
        except ValueError as e:
            date_aproved = False
    return date_ret, date_aproved


def test_type(obj_to_test, type_of):
    """
        Simple conversion and test of object type.
        usage: test_type("12", int)

        ARG:
            instance of object and type object

        RETURN:
            tuple (object, bool)

    """
    try:
        converted = isinstance(obj_to_test, type_of)
        return obj_to_test, converted
    except Exception as e:
        return None, False
