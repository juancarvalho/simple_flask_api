from flask import Flask, jsonify, request
import traceback
import logging
from datetime import date
from pymongo import MongoClient
from lib.decorators import required_data
from lib.utils import convert_str_to_date, test_type
from bson.objectid import ObjectId
from bson.json_util import dumps
import configparser

# Get config file variables ==========================================
config = configparser.ConfigParser()  # Init config object
config.read('config/settings.ini')  # Set .ini file
env = config.get('environment', 'current')  # Get current environment
url_mongo = config.get(env, 'url_mongo')
port = int(config.get(env, 'port'))
db = config.get(env, 'db')
collection = config.get(env, 'collection')

# Config mongo connection ============================================
client = MongoClient(url_mongo, port)
db = client[db]
collection = db[collection]

# Init flask object ==================================================
app = Flask(__name__)


def test_type_request(request):
    """
        Get all request data and test datatype

        ARG:
            flask request

        RETURN:
            tuple(dict, dict)
    """

    # Create data request vars
    data_ = 'data'
    hora_ = 'hora'
    inicial_ = 'containicial'
    final_ = 'contafinal'
    valor_ = 'valor'

    # dict to save in mongo
    data_to_insert = dict()

    # dict reproved types
    reproved_data = dict()

    # If request data exists
    if request.json.get(data_):
        # If request hora exists
        if request.json.get(hora_):
            # join date and hour string
            date_hour = "{0} {1}".format(
                request.json.get(data_),
                request.json.get(hora_)
            )
        else:
            # join date and hour string
            date_hour = "{0} {1}".format(
                request.json.get(data_),
                "00:00:00"
            )

        # Test data and hora formats
        data_to_insert[data_], reproved_data["data e/ou hora"] = convert_str_to_date(
            date_hour)

    # If request valor exists
    if request.json.get(valor_):
        # Test valor format
        data_to_insert[valor_], reproved_data[valor_] = test_type(
            request.json.get(valor_), int)

    return data_to_insert, reproved_data


@app.route('/api/v1/insert', methods=['POST'])
@required_data(app, 'data', 'valor')
def insert():
    """
        Insert data in database.

        ARGS:
            json object

        RETURN:
            json object
    """
    try:

        # Apply datatype tests
        data_to_insert, reproved_data = test_type_request(request)

        # Insert not verifiable datatype
        data_to_insert['containicial'] = request.json.get('containicial', None)
        data_to_insert['contafinal'] = request.json.get('contafinal', None)

        # Check if exists any wrong format
        for key, accepted in reproved_data.items():
            if not accepted:
                return jsonify({
                    'success': False,
                    'detail': 'O dado "{}" não esta em um formato aceito.'.format(key),
                    'error': None}), 400

        # pymongo insert function
        ret_id = collection.insert(data_to_insert)

        # Show log
        app.logger.info('[INSERT DATA] {}'.format(data_to_insert))

        return jsonify({
            'success': True,
            'detail': 'Objeto inserido com sucesso.',
            'object_id': str(ret_id),
            'error': None}), 201

    except Exception as e:
        return jsonify({
            'success': False,
            'detail': "Erro interno ao salvar no banco de dados",
            # 'error': str(traceback.format_exc())
            }), 500


@app.route('/api/v1/edit', methods=['PUT'])
def edit():
    """
        Update data in database

        ARGS:
            json object

        RETURN:
            json object
    """
    try:
        # Get mongo object id
        _id = request.json.get('_id')

        # Validate object id
        valid_object_id = ObjectId.is_valid(_id)

        # Check if object id is validate
        if valid_object_id:

            # Try find object mongo by _id
            ret_mongo = collection.find_one(
                {"_id": ObjectId(_id)})

            # Check if _id exists
            if ret_mongo is not None:
                # Apply datatype tests
                data_to_insert, reproved_data = test_type_request(request)

                # Update not verifiable datatype, if exists in request
                if request.json.get('containicial'):
                    data_to_insert['containicial'] = request.json.get(
                        'containicial')

                # Update not verifiable datatype, if exists in request
                if request.json.get('contafinal'):
                    data_to_insert['contafinal'] = request.json.get(
                        'contafinal')

                # Check if exists any wrong format
                for key, accepted in reproved_data.items():
                    if not accepted:
                        return jsonify({
                            'success': False,
                            'detail': 'O dado "{}" não esta em um formato aceito.'.format(key),
                            'error': None}), 400

                # Update database
                app.logger.info(data_to_insert)
                collection.update_one({'_id': ObjectId(_id)}, {
                                      '$set': data_to_insert})

                return jsonify({
                    'detail': 'Objeto {} atualizado com sucesso.'.format(_id),
                    'success': True,
                    'error': None}), 201
            else:
                return jsonify({
                    'success': False,
                    'detail': 'O _id "{}" não existe.'.format(_id),
                    'error': None}), 400
        else:
            return jsonify({
                'success': False,
                'detail': 'O _id "{}" não esta em um formato valido.'.format(_id),
                'error': None}), 400

    except Exception as e:
        return jsonify({
            'success': False,
            'detail': "Erro interno ao salvar no banco de dados.",
            # 'error': str(traceback.format_exc())
            }), 500


@app.route('/api/v1/get', methods=['GET'])
def get():
    """
        Find and return data from database

        ARGS:
            json object

        RETURN:
            json object
    """

    # Set minimal values
    date_hour = '01/01/1900 00:00:00'
    valor = 0

    # If request data exists
    if request.json.get('data'):
        # If request hora exists
        if request.json.get('hora'):
            # join date and hour string
            date_hour = "{0} {1}".format(
                request.json.get('data'),
                request.json.get('hora')
            )
        else:
            # join date and hour string
            date_hour = "{0} {1}".format(
                request.json.get('data'),
                "00:00:00"
            )

    # Get valor if exists 
    if request.json.get('valor'):
        valor = request.json.get('valor')

    # Convert data and hora formats
    date_format, aproved_format = convert_str_to_date(
        date_hour)

    # Make query and convert to list
    ret_query = list(collection.find({
            'valor': {'$gt': valor},
            'data': {'$gt': date_format}
        }))

    # Convert complex data in simple string to return
    for obj in ret_query:
        obj_data = obj['data']
        obj['data'] = obj_data.strftime("%d/%m/%Y")
        obj['hora'] = obj_data.strftime("%H:%M:%S")
        obj['containicial'] = obj['containicial']
        obj['contafinal'] = obj['contafinal']
        obj['valor'] = obj['valor']
        obj['_id'] = str(obj['_id'])

    return jsonify({
        'detail': dumps(ret_query),
        'success': True}), 200


if __name__ == '__main__':
    app.run(debug=True)
